;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(setq user-full-name "Abraham Raji"
      user-mail-address "work@abrahamr.in")

(setq-default indent-tabs-mode t)
;; Doom theme
;(setq doom-theme 'doom-tomorrow-night)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)
(use-package solar                      ; built-in
  :config
  (setq calendar-latitude 9.74
        calendar-longitude 76.70))

(setq doom-gruvbox-dark-variant "hard")
(after! solar
   (setq circadian-themes '((:sunrise . doom-tomorrow-day)
                            (:sunset  . doom-tomorrow-night)))
   (circadian-setup))


;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Cloud/Org/")
(defvar notes-directory "~/Cloud/Notes/")
(setq org-agenda-files `("~/Cloud/Org/Agenda.org" "~/Cloud/Org/Work.org"))

(defun avronr/open-dir (dir)
  "Search for a file in dir."
  (interactive)
  (doom-project-find-file 'dir))

;; (map! :leader
;;       (:prefix "o"
;;        :desc "Open notes directory" :n #'(avronr/open-dir notes-directory)))

;; Set projects file
(setq projectile-project-search-path '("~/workspace/"))

;; Set fonts
(setq doom-font (font-spec :family "FiraCode NF" :size 14 :weight 'light)
      doom-unicode-font (font-spec :family "FiraCode NF" :size 14)
      doom-variable-pitch-font (font-spec :family "Inter" :size 16)
      doom-big-font (font-spec :family "FiraCode NF" :size 19))

;; Set auth sources
(setq auth-sources '("~/.authinfo.gpg"))

;; Show gravatars in magit
(after! magit
  (setq magit-revision-show-gravatars '("^Author:     " . "^Commit:     ")))


;; Ruby specific bindings
(map! :leader
      (:prefix "m"
       :desc "Run minitest in current file" "t a" #'minitest-verify))

;; Multiple cursors
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;; company config
(after! company
  (setq company-idle-delay 0.1
        company-minimum-prefix-length 1)
  (add-hook 'evil-normal-state-entry-hook #'company-abort)) ;; make aborting less annoying.

;; org-mode formatting fix
(setq org-hide-emphasis-markers t)
(when window-system (set-frame-size (selected-frame) 120 40))
(use-package lsp-tailwindcss
  :init
  (setq lsp-tailwindcss-add-on-mode t))
(add-hook 'before-save-hook 'lsp-tailwindcss-rustywind-before-save)


(setq rmh-elfeed-org-files '("~/.doom.d/feed.org"))

(after! elfeed
  (elfeed-goodies/setup)
  (setq elfeed-search-filter "@1-month-ago +unread"))

(define-derived-mode astro-mode web-mode "astro")
(setq auto-mode-alist
      (append '((".*\\.astro\\'" . astro-mode))
              auto-mode-alist))

(with-eval-after-load 'lsp-mode
  (add-to-list 'lsp-language-id-configuration
               '(astro-mode . "astro"))

  (lsp-register-client
   (make-lsp-client :new-connection (lsp-stdio-connection '("astro-ls" "--stdio"))
                    :activation-fn (lsp-activate-on "astro")
                    :server-id 'astro-ls)))

(setq web-mode-enable-front-matter-block t)
(setq doom-gruvbox-dark-variant "dark")
