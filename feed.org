#+TITLE: Feed

* Blogs                                                              :elfeed:
** [[https://drewdevault.com/blog/index.xml][Drew DeVault's blog]] :dev:
** [[https://100r.co/links/rss.xml][Hundreds Rabbits]] :life:art:
** [[https://emersion.fr/blog/atom.xml][Emersion]] :dev:
** [[http://tonsky.me/blog/atom.xml][Tonsky]] :dev:design:
** [[https://world.hey.com/dhh/feed.atom][DHH]] :dev:entrepreneurship:
** [[https://overreacted.io/rss.xml][Dan Abramov]] :dev:
** Software Development                                                 :dev:
*** Projects
**** [[https://sourcehut.org/blog/index.xml][Sourcehut]]
**** [[https://blog.joinmastodon.org/index.xml][Mastodon]]
*** Emacs                                                    :emacs:mustread:
**** http://www.terminally-incoherent.com/blog/feed
**** http://nullprogram.com/feed
*** Weekly Updates
**** [[https://world.hey.com/this.week.in.rails/feed.atom][This week in Rails]]
**** [[https://thisweek.gnome.org/index.xml][This week in GNOME]]
**** [[https://pointieststick.com/feed/][This week in KDE]]
